#include "cpc.h"
#include <stdlib.h>
#include <stdio.h>

void main() {

	cpc::context test_context;
	test_context.malloc = malloc;
	test_context.realloc = realloc;
	test_context.free = free;
	test_context.clear = nullptr;

	cpc::push_context(test_context);
	cpc::push_context(test_context);

	cpc::dynamic_array<int> test;
	test.resize(5);
	for (int i = 0; i < 10; ++i){
		test.push(i);
	}
	auto t = test.pop();
	test.remove_at(5);
	printf("popped off = %d\n", t);
	printf("size = %u, capacity = %u\n", (unsigned)test.size, (unsigned)test.capacity);
	test.shrink();
	printf("after shrinking, size = %u, capacity = %u\n", (unsigned)test.size, (unsigned)test.capacity);
	for(int i = 0; i< test.size; ++i){
		printf("%d,",test[i]);
	}
	printf("\n");
	test.clear();
	printf("size = %u, capacity = %u\n", (unsigned)test.size, (unsigned)test.capacity);

	cpc::pop_context();
	cpc::pop_context();
	cpc::pop_context();
	cpc::pop_context();
}
