#pragma once

namespace cpc {

	typedef void* (*malloc_func)(size_t);
	typedef void* (*realloc_func)(void*,size_t);
	typedef void (*free_func)(void*);
	typedef void (*clear_func)(void);

	struct context {
		malloc_func malloc = nullptr;
		realloc_func realloc = nullptr;
		free_func free = nullptr;
		clear_func clear = nullptr;
	};

	bool push_context(context);
	void pop_context();
	context* current_context();

	template <typename F>
		struct privDefer {
			F f;
			privDefer(F f) : f(f) {}
			~privDefer() { f(); }
		};

	template <typename F>
		privDefer<F> defer_func(F f) {
			return privDefer<F>(f);
		}

	constexpr unsigned growth_factor = 2;

	template<typename T>
		struct dynamic_array {
			T* data = nullptr;
			size_t size = 0;
			size_t capacity = 0;

			T& operator[](size_t index) {
				return data[index];
			}

			bool push(T elem) {
				auto cnxt = current_context();
				if (!cnxt) return false;
				if (size >= capacity){
					auto new_capacity = capacity*growth_factor;
					auto new_data = (T*) cnxt->realloc(data, new_capacity * sizeof(T));
					if (new_data == nullptr) return false;
					data = new_data;
					capacity = new_capacity;
				}

				data[size++] = elem;
				return true;
			}

			void clear(){
				auto cnxt = current_context();
				if (!cnxt) return;
				size = 0;
				capacity = 0;
				cnxt->free(data);
				data = nullptr;
			}

			bool resize(size_t new_capacity){
				auto cnxt = current_context();
				if (!cnxt) return false;
				if (new_capacity == 0) {
					clear();
					return true;
				}

				auto new_data = (T*)cnxt->realloc(data, new_capacity * sizeof(T));
				if (new_data == nullptr) return false;
				data = new_data;
				capacity = new_capacity;
				size = new_capacity < size ? new_capacity : size;
				return true;
			}

			bool shrink(){
				return resize(size);
			}

			void remove_at(size_t index) {
				for(int i = index; i + 1 < size; i++){
					data[i] = data[i+1];
				}
				--size;
			}

			T pop(){
				return data[--size];
			}
		};

	using string = dynamic_array<char>;
	constexpr string literal_to_string(const char* str) {
		return {};
	}
}

#define DEFER_1(x, y) x##y
#define DEFER_2(x, y) DEFER_1(x, y)
#define DEFER_3(x) DEFER_2(x, __COUNTER__)
#define cpc_defer(code)  auto DEFER_3(_defer_) = cpc::defer_func([&]code);

#ifdef CPC_IMPL
namespace cpc {
	namespace {
		struct context_stack {
			cpc::context current;
			context_stack* prev = nullptr;
		};

		context_stack base_node;
		context_stack *current_node = nullptr;
	}

	bool push_context(context cnxt) {
		context_stack *node = nullptr;
		context_stack *prev = nullptr;

		if (current_node != nullptr){
			prev = current_node;
			node = (context_stack*)current_node->current.malloc(sizeof(context_stack));
			if (node == nullptr) return false;
		} else {
			node = &base_node;
		}

		node->current.malloc = cnxt.malloc;
		node->current.realloc = cnxt.realloc;
		node->current.free = cnxt.free;
		node->current.clear = cnxt.clear;
		node->prev = prev;
		current_node = node;

		return true;
	}

	void pop_context(){
		if (!current_node) return;

		if (current_node == &base_node){
			current_node = nullptr;
			base_node = {0};
			return;
		}

		auto prev_node = current_node->prev;
		prev_node->current.free(current_node);
		current_node = prev_node;
	}

	context* current_context(){
		if (current_node == nullptr) return nullptr;
		return &(current_node->current);
	}
}

#endif
